﻿using System.Xml;

namespace PRNews.Feeds;

/// <summary>
/// Reads an Atom feed
/// </summary>
internal static class FeedReader
{
    /// <summary>Value when non avaiable</summary>
    private const string NaValue = "N/A";

    public static FeedItem[] Read(string url)
    {
        var atomFeed = new XmlDocument();
        atomFeed.Load(url);

        // Select all entry nodes
        var entryNodes = atomFeed.GetElementsByTagName("entry");

        var span = new FeedItem[entryNodes.Count];

        // Loop through each entry
        for (var i = 0; i < entryNodes.Count; i++)
        {
            var entryNode = entryNodes[i];
            var title = GetChildNodeValue(entryNode, "title");
            var publishDate = GetChildNodeValue(entryNode, "published");
            var link = GetAttributeFromChildNode(entryNode, "link", "href");
            var category = GetAttributeFromChildNode(entryNode, "category", "term");

            span[i] = new FeedItem(title, category, link, DateTimeOffset.Parse(publishDate));
        }

        return span;
    }

    // Helper method to get the value of a child node by name
    static string GetChildNodeValue(XmlNode parentNode, string childNodeName)
    {
        XmlNode? childNode = parentNode[childNodeName];
        return childNode != null ? childNode.InnerText : NaValue;
    }

    // Helper method to get an attribute's value from a child node
    static string GetAttributeFromChildNode(XmlNode parentNode, string childNodeName, string attributeName)
    {
        XmlNode? childNode = parentNode[childNodeName];
        if (childNode is not { Attributes: not null })
        {
            return NaValue;
        }

        var attribute = childNode.Attributes[attributeName];
        return attribute != null ? attribute.Value : NaValue;
    }

    public record struct FeedItem(string Title, string Category, string Url, DateTimeOffset PublishDate);
}