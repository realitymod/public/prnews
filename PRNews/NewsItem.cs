﻿namespace PRNews;
using System.Text.Json.Serialization;

internal sealed class NewsItem
{
    [JsonIgnore]
    public DateTime Date { get; init; }

    public string Text { get; init; } = string.Empty;

    public string Url { get; init; } = string.Empty;

    public string Color { get; init; } = string.Empty;

    /// <inheritdoc />
    public override string ToString() => string.Format("{1}{0}{2}{0}{3}{0}{4}", Environment.NewLine, this.Text, this.Url, this.Color, this.Date);
}