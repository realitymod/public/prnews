﻿namespace PRNews;
using System.Text.Json.Serialization;

[JsonSourceGenerationOptions(WriteIndented = false)]
[JsonSerializable(typeof(NewsItem))]
[JsonSerializable(typeof(NewsItem[]))]
internal partial class PrNewsJsonSerializerContext : JsonSerializerContext
{
}