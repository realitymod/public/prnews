﻿using System.Globalization;
using System.Text.Json;
using PRNews.Feeds;

namespace PRNews
{
    /// <summary>Writes Ticket Json</summary>
    internal static class TickerWriter
    {
        public static async Task WriteAsync(FeedReader.FeedItem[] items, string path)
        {
            var tickerItems = new NewsItem[items.Length];

            for (var i = 0; i < items.Length; i++)
            {
                var item = items[i];
                var text = item.Title;
                var color = ChooseColor(item);
                var publishDate = item.PublishDate;
                var date = publishDate.UtcDateTime;
                var newsItem = new NewsItem
                {
                    Date = publishDate.UtcDateTime,
                    Text = $"{date.ToString("dd-MMM-yy", CultureInfo.InvariantCulture)} - {text}",
                    Url = item.Url,
                    Color = color
                };
                tickerItems[i] = newsItem;
            }

            await using var stream = File.Create(path);
            await JsonSerializer.SerializeAsync(stream, tickerItems, PrNewsJsonSerializerContext.Default.NewsItemArray);
        }

        private static string ChooseColor(FeedReader.FeedItem item)
        {
            var category = item.Category;

            if (string.Equals(category, "Announcements & Highlights", StringComparison.OrdinalIgnoreCase))
            {
                return "#ff0000";
            }

            if (string.Equals(category, "Development Blogs", StringComparison.OrdinalIgnoreCase))
            {
                return "#3399ff";
            }

            if (string.Equals(category, "Changelogs", StringComparison.OrdinalIgnoreCase))
            {
                return "#ffcc33";
            }

            return "#999999";
        }
    }
}
