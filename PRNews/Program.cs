﻿using PRNews;
using PRNews.Feeds;

var url = "https://forum.realitymod.com/feed.php?mode=news";
var feed = FeedReader.Read(url);

await TickerWriter.WriteAsync(feed, "newsticker.json");
